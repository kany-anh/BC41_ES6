const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

const loadMau = () => {
  let bangMau = "";
  colorList.forEach((item) => {
    bangMau += `<button  class= "color-button ${item} " ></button>`;
  });
  return (document.getElementById("colorContainer").innerHTML = bangMau);
};
loadMau();

const themActive = () => {
  let chon = document.querySelectorAll(".color-button");
  console.log(chon);
  chon.forEach((item) => {
    item.addEventListener("click", () => {
      let house = document.getElementById("house");
      house.className = "house " + item.classList[1];
      console.log(item.classList);
      item.classList.add("active");
      chon.forEach((elm) => {
        if (elm !== item) {
          elm.classList.remove("active");
          // console.log("item", item);
          // console.log("elm", elm);
        }
      });
    });
  });
};
themActive();

// const chonMau = () => {
//   let house = document.getElementById("house");
//   colorList.forEach((item) => {
//     if (house.className == "house") {
//       element.className = "newStyle";
//     }
//   });
// };
